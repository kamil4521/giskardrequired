using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsTrueWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfConditionTrue()
        {
            Assert.DoesNotThrow(delegate { Require.IsTrue<ArgumentException>(0 == 0, () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfConditionFalse()
        {
            try
            {
                Require.IsTrue<ArgumentException>(0 == 1, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}