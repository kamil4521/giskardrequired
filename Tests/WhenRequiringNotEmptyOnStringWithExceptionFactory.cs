using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnStringWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfStringNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.NotEmpty<ArgumentException>("aaaa", () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfStringIsEmpty()
        {
            try
            {
                Require.NotEmpty<ArgumentException>("", () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }

        [Test]
        public void ShouldThrowIfStringIsNull()
        {
            try
            {
                string str = null;
                Require.NotEmpty<ArgumentException>(str, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}