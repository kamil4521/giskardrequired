using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringAllNotEmptyOnObject
    {
        [Test]
        public void ShouldNotThrowIfObjectsNotEmpty()
        {
            Assert.DoesNotThrow(() => Require.AllNotEmpty<ArgumentException>(new object[] { new { } }));
        }

        [Test]
        public void ShouldThrowIfOneObjectIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => Require.AllNotEmpty<ArgumentException>(new object[] { null }));
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.AllNotEmpty<ArgumentException>(new object[] { null }, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}