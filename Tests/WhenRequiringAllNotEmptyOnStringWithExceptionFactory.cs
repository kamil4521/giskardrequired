using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringAllNotEmptyOnStringWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfStringsNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.AllNotEmpty<ArgumentException>(new string[] { "aaaa" }, () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfOneStringIsEmpty()
        {
            try
            {
                Require.AllNotEmpty<ArgumentException>(new string[] { "" }, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }

        [Test]
        public void ShouldThrowIfOneStringIsNull()
        {
            try
            {
                Require.AllNotEmpty<ArgumentException>(new string[] { null }, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}