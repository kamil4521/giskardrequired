using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionIsNotNullOrEmpty
    {
        [Test]
        public void ShouldNotThrowIfCollectionContainsItems()
        {
            Assert.DoesNotThrow(() => Require.NotEmpty<ArgumentException>(new List<object> { 1, 2 }));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsNull()
        {
            Assert.Catch<ArgumentException>(() => Require.NotEmpty<ArgumentException>((IEnumerable<object>)null));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsEmpty()
        {
            Assert.Catch<ArgumentException>(() => Require.NotEmpty<string, ArgumentException>(new List<string>()));
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.NotEmpty<string, ArgumentException>(new List<string>(), message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}