using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnString
    {
        [Test]
        public void ShouldNotThrowIfStringNotEmpty()
        {
            Assert.DoesNotThrow(() => Require.NotEmpty<ArgumentException>("aaaa"));
        }

        [Test]
        public void ShouldThrowIfStringIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => Require.NotEmpty<ArgumentException>(""));
        }

        [Test]
        public void ShouldThrowIfStringIsNull()
        {
            string str = null;
            Assert.Throws<ArgumentException>(() => Require.NotEmpty<ArgumentException>(str));
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.NotEmpty<ArgumentException>(string.Empty, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}