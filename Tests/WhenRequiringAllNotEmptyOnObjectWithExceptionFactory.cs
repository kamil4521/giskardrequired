using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringAllNotEmptyOnObjectWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfObjectsNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.AllNotEmpty<ArgumentException>(new object[] { new { } }, () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfOneObjectIsEmpty()
        {
            try
            {
                Require.AllNotEmpty<ArgumentException>(new object[] { null }, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}