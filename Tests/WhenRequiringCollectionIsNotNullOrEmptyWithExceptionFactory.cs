using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionIsNotNullOrEmptyWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfCollectionContainsItems()
        {
            Assert.DoesNotThrow(() => Require.NotEmpty(new List<object> { 1, 2 }, () => new ArgumentException("test")));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsNull()
        {
            Assert.Catch<ArgumentException>(() => Require.NotEmpty((IEnumerable<object>)null, () => new ArgumentException("test")));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsEmpty()
        {
            Assert.Catch<ArgumentException>(() => Require.NotEmpty(new List<object>(), () => new ArgumentException("test")));
        }
    }
}