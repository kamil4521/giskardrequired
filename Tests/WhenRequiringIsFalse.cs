using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsFalse
    {
        [Test]
        public void ShouldNotThrowIfConditionFalse()
        {
            Assert.DoesNotThrow(delegate { Require.IsFalse<ArgumentException>(0 == 1); });
        }

        [Test]
        public void ShouldThrowIfConditionTrue()
        {
            Assert.Throws<ArgumentException>(delegate { Require.IsFalse<ArgumentException>(0 == 0); });
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.IsFalse<ArgumentException>(true, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}