using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsLessThenZero
    {
        [Test]
        public void ShouldNotThrowIfNumberIsLessThenZero()
        {
            Assert.DoesNotThrow(() => Require.LessThenZero<ArgumentException>(-1));
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public void ShouldThrowIfNumberIsNotLessThenZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.LessThenZero<ArgumentException>(value));
        }
    }
}