using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnObjectWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfObjectNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.NotEmpty<ArgumentException>(new { }, () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfObjectIsEmpty()
        {
            try
            {
                object obj = null;
                Require.NotEmpty<ArgumentException>(obj, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}