using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringDoesNotThrow
    {
        [Test]
        public void ShouldNotThrowIfConditionTrue()
        {
            Assert.DoesNotThrow(delegate { Require.DoesNotThrow<ArgumentException>(() => { }, (inner) => new ArgumentException("test", inner)); });
        }

        [Test]
        public void ShouldThrowIfConditionFalse()
        {
            try
            {
                Require.DoesNotThrow<ArgumentException>(() => { throw new InvalidOperationException(); }, (inner) => new ArgumentException("test", inner));
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
                Assert.IsNotNull(ex.InnerException);
                Assert.IsInstanceOf<InvalidOperationException>(ex.InnerException);
            }
        }
    }
}