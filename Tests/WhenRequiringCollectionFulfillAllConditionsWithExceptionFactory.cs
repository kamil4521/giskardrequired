﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionFulfillAllConditionsWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowWhenAllItemsFullfillAllConditions()
        {
            Assert.DoesNotThrow(() => Require.Each(new List<int>(), () => new ArgumentException("test"), n => n > 3, n => n % 2 == 0));
            Assert.DoesNotThrow(() => Require.Each(new List<int> { 4, 6, 10 }, () => new ArgumentException("test"), n => n > 3, n => n % 2 == 0));
        }

        [Test]
        public void ShouldThrowWhenAnyItemNotFulfillAnyConditions()
        {
            Assert.Catch<ArgumentException>(() => Require.Each(new List<int> { 4, 2, 10 }, () => new ArgumentException("test"), n => n > 3, n => n % 2 == 0));
            Assert.Catch<ArgumentException>(() => Require.Each(new List<int> { 4, 5, 10 }, () => new ArgumentException("test"), n => n > 3, n => n % 2 == 0));
        }
    }
}