﻿using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnGuidWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfGuidNotEmpty()
        {
            Assert.DoesNotThrow(() => Require.NotEmpty(Guid.NewGuid(), () => new ArgumentException("test")));
        }

        [Test]
        public void ShouldThrowIfGuidIsEmpty()
        {
            try
            {
                Require.NotEmpty(Guid.Empty, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}
