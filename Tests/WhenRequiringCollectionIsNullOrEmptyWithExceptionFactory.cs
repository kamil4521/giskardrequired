using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionIsNullOrEmptyWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfCollectionIsNullOrEmpty()
        {
            Assert.DoesNotThrow(() => Require.IsEmpty(new List<object>(), () => new ArgumentException("test")));
            Assert.DoesNotThrow(() => Require.IsEmpty<object, ArgumentException>(null, () => new ArgumentException("test")));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsNotNullOrEmpty()
        {
            Assert.Catch<ArgumentException>(() => Require.IsEmpty<object, ArgumentException>(new List<string> { "a", "b" }, () => new ArgumentException("test")));
        }
    }
}