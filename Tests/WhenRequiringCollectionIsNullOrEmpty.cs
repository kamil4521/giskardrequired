﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionIsNullOrEmpty
    {
        [Test]
        public void ShouldNotThrowIfCollectionIsNullOrEmpty()
        {
            Assert.DoesNotThrow(() => Require.IsEmpty<object, ArgumentException>(new List<object>()));
            Assert.DoesNotThrow(() => Require.IsEmpty<object, ArgumentException>((IEnumerable<Object>)null));
        }

        [Test]
        public void ShouldThrowWhenCollectionIsNotNullOrEmpty()
        {
            Assert.Catch<ArgumentException>(() => Require.IsEmpty<int, ArgumentException>(new List<int> { 1 }));
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.IsEmpty<int, ArgumentException>(new List<int>() { 1 }, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}