using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsZero
    {
        [Test]
        public void ShouldNotThrowIfNumberIsZero()
        {
            Assert.DoesNotThrow(() => Require.IsZero<ArgumentException>(0));
        }

        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void ShouldThrowIfNumberIsNotZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.IsZero<ArgumentException>(value));
        }
    }
}