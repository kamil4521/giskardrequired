using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringAllNotEmptyOnString
    {
        [Test]
        public void ShouldNotThrowIfStringsNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.AllNotEmpty<ArgumentException>(new string[] { "aaaa" }); });
        }

        [Test]
        public void ShouldThrowIfOneStringsIsEmpty()
        {
            Assert.Throws<ArgumentException>(delegate { Require.AllNotEmpty<ArgumentException>(new string[] { "" }); });
        }

        [Test]
        public void ShouldThrowIfOneStringIsNull()
        {
            Assert.Throws<ArgumentException>(delegate { Require.AllNotEmpty<ArgumentException>(new string[] { null }); });
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.AllNotEmpty<ArgumentException>(new string[] { null }, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}