﻿using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringEmailWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfEmail()
        {
            Assert.DoesNotThrow(delegate { Require.IsEmail<ArgumentException>("test@user.com", () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfStringIsEmpty()
        {
            try
            {
                Require.IsEmail<ArgumentException>("", () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }

        [Test]
        public void ShouldThrowIfStringIsNull()
        {
            try
            {
                string str = null;
                Require.IsEmail<ArgumentException>(str, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }

        [Test]
        public void ShouldThrowIfStringIsNotValidEmail()
        {
            try
            {
                Require.IsEmail<ArgumentException>("invalid", () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}