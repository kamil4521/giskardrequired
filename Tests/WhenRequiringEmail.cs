using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringEmail
    {
        [Test]
        public void ShouldNotThrowIfEmail()
        {
            Assert.DoesNotThrow(delegate { Require.IsEmail<ArgumentException>("test@user.com"); });
        }

        [Test]
        public void ShouldThrowIfStringIsEmpty()
        {
            Assert.Throws<ArgumentException>(delegate { Require.IsEmail<ArgumentException>(""); });
        }

        [Test]
        public void ShouldThrowIfStringIsNull()
        {
            string str = null;
            Assert.Throws<ArgumentException>(delegate { Require.IsEmail<ArgumentException>(str); });
        }

        [Test]
        public void ShouldThrowIfStringIsNotValidEmail()
        {
            Assert.Throws<ArgumentException>(delegate { Require.IsEmail<ArgumentException>("invalid"); });
        }
    }
}