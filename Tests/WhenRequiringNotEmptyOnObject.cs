using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnObject
    {
        [Test]
        public void ShouldNotThrowIfObjectNotEmpty()
        {
            Assert.DoesNotThrow(delegate { Require.NotEmpty<ArgumentException>(new { }); });
        }

        [Test]
        public void ShouldThrowIfObjectIsEmpty()
        {
            object obj = null;
            Assert.Throws<ArgumentException>(delegate { Require.NotEmpty<ArgumentException>(obj); });
        }
    }
}