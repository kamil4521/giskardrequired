using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsTrue
    {
        [Test]
        public void ShouldNotThrowIfConditionTrue()
        {
            Assert.DoesNotThrow(delegate { Require.IsTrue<ArgumentException>(0 == 0); });
        }

        [Test]
        public void ShouldThrowIfConditionFalse()
        {
            Assert.Throws<ArgumentException>(delegate { Require.IsTrue<ArgumentException>(0 == 1); });
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.IsTrue<ArgumentException>(0 == 1, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}