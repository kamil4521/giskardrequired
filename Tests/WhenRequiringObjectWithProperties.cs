using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringObjectWithProperties
    {
        private class ForTest
        {
            public string Property { get; set; }
            public int Value { get; set; }
        }

        [Test]
        public void ShouldNotThrowWhenPropertyExist()
        {
            Assert.DoesNotThrow(() => Require.HasProperties<ArgumentException>(new { value = 1 }, "value"));
            Assert.DoesNotThrow(() => Require.HasProperties<ArgumentException>(new { value = 1, prop = "test" }, "value"));
            Assert.DoesNotThrow(() => Require.HasProperties<ArgumentException>(new { value = 1, prop = "test" }, "value", "prop"));

            Assert.DoesNotThrow(() => Require.HasProperties<ArgumentException>(new ForTest(), "Property"));
            Assert.DoesNotThrow(() => Require.HasProperties<ArgumentException>(new ForTest(), "Property", "Value"));
        }

        [Test]
        public void ShouldThrowWhenObjectNotHasProperty()
        {
            Assert.Catch<ArgumentException>(() => Require.HasProperties<ArgumentException>(new { value = 1 }, "sample"));
            Assert.Catch<ArgumentException>(() => Require.HasProperties<ArgumentException>(new { value = 1 }, "value", "sample"));

            Assert.Catch<ArgumentException>(() => Require.HasProperties<ArgumentException>(new ForTest(), "Property", "SecondProperty"));
            Assert.Catch<ArgumentException>(() => Require.HasProperties<ArgumentException>(new ForTest(), "WrongProperty"));
        }

        [Test]
        public void ShouldThrowWhenPropertyIsNullOrEmpty()
        {
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(new { value = 1 }));
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(new { value = 1 }, new string[0]));

            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(new ForTest()));
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(new ForTest(), new string[0]));
        }

        [Test]
        public void ShoulThrowWhenObjectIsNull()
        {
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(null));
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(null, new string[0]));
            Assert.Catch<ArgumentNullException>(() => Require.HasProperties<ArgumentException>(null, "test"));
        }
    }
}