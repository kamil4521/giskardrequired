using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsFalseWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfConditionFalse()
        {
            Assert.DoesNotThrow(delegate { Require.IsFalse<ArgumentException>(0 == 1, () => new ArgumentException("test")); });
        }

        [Test]
        public void ShouldThrowIfConditionTrue()
        {
            try
            {
                Require.IsFalse<ArgumentException>(0 == 0, () => new ArgumentException("test"));
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<ArgumentException>(ex);
                Assert.AreEqual("test", ex.Message);
            }
        }
    }
}