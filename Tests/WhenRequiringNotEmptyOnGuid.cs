﻿using NUnit.Framework;
using System;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringNotEmptyOnGuid
    {
        [Test]
        public void ShouldNotThrowIfGuidNotEmpty()
        {
            Assert.DoesNotThrow(() => Require.NotEmpty<ArgumentException>(Guid.NewGuid()));
        }

        [Test]
        public void ShouldThrowIfGuidIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => Require.NotEmpty<ArgumentException>(Guid.Empty));
        }

        [Test]
        public void ShouldThrowExceptionWithSpecificErrorMessage()
        {
            const string message = "error_message";
            try
            {
                Require.NotEmpty<ArgumentException>(Guid.Empty, message);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}
