using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringCollectionFulfillAllConditions
    {
        [Test]
        public void ShouldNotThrowWhenAllItemsFullfillAllConditions()
        {
            Assert.DoesNotThrow(() => Require.Each<int, ArgumentException>(new List<int>(), n => n > 3, n => n % 2 == 0));
            Assert.DoesNotThrow(() => Require.Each<int, ArgumentException>(new List<int> { 4, 6, 10 }, n => n > 3, n => n % 2 == 0));
        }

        [Test]
        public void ShouldThrowWhenAnyItemNotFulfillAnyConditions()
        {
            Assert.Catch<ArgumentException>(() => Require.Each<int, ArgumentException>(new List<int> { 4, 2, 10 }, n => n > 3, n => n % 2 == 0));
            Assert.Catch<ArgumentException>(() => Require.Each<int, ArgumentException>(new List<int> { 4, 5, 10 }, n => n > 3, n => n % 2 == 0));
        }
    }
}