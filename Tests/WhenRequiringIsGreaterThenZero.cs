using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsGreaterThenZero
    {
        [Test]
        public void ShouldNotThrowIfNumberIsGreaterThenZero()
        {
            Assert.DoesNotThrow(() => Require.GreaterThenZero<ArgumentException>(1));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void ShouldThrowIfNumberIsNotGreaterThenZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.GreaterThenZero<ArgumentException>(value));
        }
    }
}