using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsLessThenZeroWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfNumberIsLessThenZero()
        {
            Assert.DoesNotThrow(() => Require.LessThenZero(-1, () => new ArgumentException("test")));
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public void ShouldThrowIfNumberIsNotLessThenZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.LessThenZero(value, () => new ArgumentException("test")));
        }
    }
}