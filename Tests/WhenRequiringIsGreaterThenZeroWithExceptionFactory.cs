using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsGreaterThenZeroWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfNumberIsGreaterThenZero()
        {
            Assert.DoesNotThrow(() => Require.GreaterThenZero(1, () => new ArgumentException("test")));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void ShouldThrowIfNumberIsNotGreaterThenZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.GreaterThenZero(value, () => new ArgumentException("test")));
        }
    }
}