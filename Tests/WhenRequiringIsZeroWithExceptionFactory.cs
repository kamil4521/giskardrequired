using System;
using NUnit.Framework;

namespace GiskardSolutions.Conditionals.Tests
{
    [TestFixture]
    public class WhenRequiringIsZeroWithExceptionFactory
    {
        [Test]
        public void ShouldNotThrowIfNumberIsZero()
        {
            Assert.DoesNotThrow(() => Require.IsZero(0, () => new ArgumentException("test")));
        }

        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void ShouldThrowIfNumberIsNotZero(int value)
        {
            Assert.Catch<ArgumentException>(() => Require.IsZero(value, () => new ArgumentException("test")));
        }
    }
}