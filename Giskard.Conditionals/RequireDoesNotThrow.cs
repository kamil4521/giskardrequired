﻿using System;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// Requires that the given action should not throw an exception.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="action">The requirement is satisfied if this does not throw.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied. The argument should be set as InnerException.</param>
        public static void DoesNotThrow<TException>(Action action, Func<Exception, TException> exceptionFactory) where TException : Exception
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                throw exceptionFactory(ex);
            }
        }
    }
}
