﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// Requires that the given string should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null nor empty.</param>
        public static void NotEmpty<TException>(string candidate) where TException : Exception, new()
        {
            if (string.IsNullOrEmpty(candidate))
                throw new TException();
        }

        /// <summary>
        /// Requires that the given string should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null nor empty.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void NotEmpty<TException>(string candidate, string message) where TException : Exception
        {
            if (string.IsNullOrEmpty(candidate))
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that the given string should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null nor empty.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void NotEmpty<TException>(string candidate, Func<TException> exceptionFactory) where TException : Exception
        {
            if (string.IsNullOrEmpty(candidate))
                throw exceptionFactory();
        }

        /// <summary>
        /// Requires that the given object should not be null.
        /// </summary>
        /// <typeparam name="TException"The exception to throw if the requirement is not satisfied.></typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null.</param>
        public static void NotEmpty<TException>(object candidate) where TException : Exception, new()
        {
            if (candidate == null)
                throw new TException();
        }

        /// <summary>
        /// Requires that the given object should not be null.
        /// </summary>
        /// <typeparam name="TException"The exception to throw if the requirement is not satisfied.></typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void NotEmpty<TException>(object candidate, string message) where TException : Exception
        {
            if (candidate == null)
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that the given object should not be null.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not null.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void NotEmpty<TException>(object candidate, Func<TException> exceptionFactory) where TException : Exception
        {
            if (candidate == null)
                throw exceptionFactory();
        }

        /// <summary>
        /// Requires that the given guid is not empty (i.e. equal to {00000000-0000-0000-0000-000000000000}).
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not {00000000-0000-0000-0000-000000000000}.</param>
        public static void NotEmpty<TException>(Guid candidate) where TException : Exception, new()
        {
            if (candidate.Equals(Guid.Empty))
                throw new TException();
        }

        /// <summary>
        /// Requires that the given guid is not empty (i.e. equal to {00000000-0000-0000-0000-000000000000}).
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not {00000000-0000-0000-0000-000000000000}.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void NotEmpty<TException>(Guid candidate, string message) where TException : Exception
        {
            if (candidate.Equals(Guid.Empty))
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that the given guid is not empty (i.e. equal to {00000000-0000-0000-0000-000000000000}).
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidate">The requirement is satisfied if this is not {00000000-0000-0000-0000-000000000000}.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void NotEmpty<TException>(Guid candidate, Func<TException> exceptionFactory) where TException : Exception
        {
            if (candidate.Equals(Guid.Empty))
                throw exceptionFactory();
        }

        /// <summary>
        /// Requires that collection is not null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is not null nor empty.</param>
        public static void NotEmpty<TSource, TException>(IEnumerable<TSource> colletion) where TException : Exception, new()
        {
            if (colletion == null || !colletion.Any())
                throw new TException();
        }

        /// <summary>
        /// Requires that collection is not null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is not null nor empty.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void NotEmpty<TSource, TException>(IEnumerable<TSource> colletion, string message) where TException : Exception
        {
            if (colletion == null || !colletion.Any())
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that collection is not null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is not null nor empty.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void NotEmpty<TSource, TException>(IEnumerable<TSource> colletion, Func<TException> exceptionFactory) where TException : Exception
        {
            if (colletion == null || !colletion.Any())
                throw exceptionFactory();
        }

        /// <summary>
        /// Requires that all strings in the given collection should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all objects in the collection are not null nor empty.</param>
        public static void AllNotEmpty<TException>(IEnumerable<string> candidates) where TException : Exception, new()
        {
            if (candidates.Any(string.IsNullOrEmpty))
                throw new TException();
        }

        /// <summary>
        /// Requires that all strings in the given collection should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all objects in the collection are not null nor empty.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void AllNotEmpty<TException>(IEnumerable<string> candidates, string message) where TException : Exception
        {
            if (candidates.Any(string.IsNullOrEmpty))
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that all strings in the given collection should not be null nor empty.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all objects in the collection are not null nor empty.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void AllNotEmpty<TException>(IEnumerable<string> candidates, Func<TException> exceptionFactory) where TException : Exception
        {
            if (candidates.Any(string.IsNullOrEmpty))
                throw exceptionFactory();
        }

        /// <summary>
        /// Requires that all object in the given collection should not be null.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all object in this collection are not null.</param>
        public static void AllNotEmpty<TException>(IEnumerable<object> candidates) where TException : Exception, new()
        {
            if (candidates.Any(c => c == null))
                throw new TException();
        }

        /// <summary>
        /// Requires that all object in the given collection should not be null.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all object in this collection are not null.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void AllNotEmpty<TException>(IEnumerable<object> candidates, string message) where TException : Exception
        {
            if (candidates.Any(c => c == null))
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that all object in the given collection should not be null.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all object in this collection are not null.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void AllNotEmpty<TException>(IEnumerable<object> candidates, Func<TException> exceptionFactory) where TException : Exception
        {
            if (candidates.Any(c => c == null))
                throw exceptionFactory();
        }
    }
}
