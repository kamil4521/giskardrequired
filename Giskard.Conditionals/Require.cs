﻿using System;
using System.Linq;
using System.Net.Mail;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// Requires that all object in the given collection should not be null.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="object">The requirement is satisfied if this is not null.</param>
        /// <param name="properties">The requirement is satisfied if this is not null nor empty.</param>
        public static void HasProperties<TException>(object @object, params string[] properties) where TException : Exception, new()
        {
            if (@object == null)
                throw new ArgumentNullException("\"object\" cannot be null");

            if (properties == null || !properties.Any())
                throw new ArgumentNullException("\"properties\" connot be null or empty");

            foreach (var property in properties)
            {
                if (@object.GetType().GetProperty(property) == null)
                    throw (TException)Activator.CreateInstance(typeof(TException), property);
            }
        }



        /// <summary>
        /// Requires that the given string should be a valid e-mail address.
        /// </summary>
        /// <typeparam name="TException"The exception to throw if the requirement is not satisfied.></typeparam>
        /// <param name="candidate">The requirement is satisfied if this is a valid e-mail address.</param>
        public static void IsEmail<TException>(string candidate) where TException : Exception, new()
        {
            if (string.IsNullOrEmpty(candidate))
                throw new TException();

            try
            {
                new MailAddress(candidate);
            }
            catch (FormatException)
            {
                throw new TException();
            }
        }

        /// <summary>
        /// Requires that the given string should be a valid e-mail address.
        /// </summary>
        /// <typeparam name="TException"The exception to throw if the requirement is not satisfied.></typeparam>
        /// <param name="candidate">The requirement is satisfied if this is a valid e-mail address.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void IsEmail<TException>(string candidate, Func<TException> exceptionFactory) where TException : Exception
        {
            if (string.IsNullOrEmpty(candidate))
                throw exceptionFactory();

            try
            {
                new MailAddress(candidate);
            }
            catch (FormatException)
            {
                throw exceptionFactory();
            }
        }
    }
}
