﻿using System;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// The requirement is satisfied when the given condition is true.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is true.</param>
        public static void IsTrue<TException>(bool condition) where TException : Exception, new()
        {
            IsTrue(condition, () => new TException());
        }

        /// <summary>
        /// The requirement is satisfied when the given condition is true.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is true.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void IsTrue<TException>(bool condition, string message) where TException : Exception
        {
            IsTrue(condition, () => (TException)Activator.CreateInstance(typeof(TException), message));
        }

        /// <summary>
        /// The requirement is satisfied when the given condition is true.
        /// </summary>
        /// <typeparam name="TException">he exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is true.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void IsTrue<TException>(bool condition, Func<TException> exceptionFactory) where TException : Exception
        {
            if (!condition)
            {
                throw exceptionFactory();
            }
        }
    }
}
