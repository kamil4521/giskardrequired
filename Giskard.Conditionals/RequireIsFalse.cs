﻿using System;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// The requirement is satisfied when the given condition is false.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is false.</param>
        public static void IsFalse<TException>(bool condition) where TException : Exception, new()
        {
            IsFalse(condition, () => new TException());
        }

        /// <summary>
        /// The requirement is satisfied when the given condition is false.
        /// </summary>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is false.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void IsFalse<TException>(bool condition, string message) where TException : Exception
        {
            IsFalse(condition, () => (TException)Activator.CreateInstance(typeof(TException), message));
        }

        /// <summary>
        /// The requirement is satisfied when the given condition is false.
        /// </summary>
        /// <typeparam name="TException">he exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="condition">The requirement is satisfied if this is false.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void IsFalse<TException>(bool condition, Func<TException> exceptionFactory) where TException : Exception
        {
            if (condition)
            {
                throw exceptionFactory();
            }
        }
    }
}
