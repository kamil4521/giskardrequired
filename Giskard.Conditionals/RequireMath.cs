﻿using System;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// The requirement is satisfied when the given number not equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        public static void NotZero<TException>(int number) where TException : Exception, new()
        {
            if (number == 0)
                throw new TException();
        }

        /// <summary>
        /// The requirement is satisfied when the given number not equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="message">The message that describes the error.</param>
        public static void NotZero<TException>(int number, string message) where TException : Exception
        {
            if (number == 0)
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// The requirement is satisfied when the given number not equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void NotZero<TException>(int number, Func<TException> exceptionFactory) where TException : Exception
        {
            if (number == 0)
                throw exceptionFactory();
        }

        /// <summary>
        /// The requirement is satisfied when the given number equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        public static void IsZero<TException>(int number) where TException : Exception, new()
        {
            if (number != 0)
                throw new TException();
        }

        /// <summary>
        /// The requirement is satisfied when the given number equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="message">The message that describes the error.</param>
        public static void IsZero<TException>(int number, string message) where TException : Exception
        {
            if (number != 0)
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// The requirement is satisfied when the given number equal to zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void IsZero<TException>(int number, Func<TException> exceptionFactory) where TException : Exception
        {
            if (number != 0)
                throw exceptionFactory();
        }

        /// <summary>
        /// The requirement is satisfied when the given number is greater then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        public static void GreaterThenZero<TException>(int number) where TException : Exception, new()
        {
            if (number <= 0)
                throw new TException();
        }

        /// <summary>
        /// The requirement is satisfied when the given number is greater then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="message">The message that describes the error.</param>
        public static void GreaterThenZero<TException>(int number, string message) where TException : Exception
        {
            if (number <= 0)
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// The requirement is satisfied when the given number is greater then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void GreaterThenZero<TException>(int number, Func<TException> exceptionFactory) where TException : Exception
        {
            if (number <= 0)
                throw exceptionFactory();
        }

        /// <summary>
        /// The requirement is satisfied when the given number is less then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        public static void LessThenZero<TException>(int number) where TException : Exception, new()
        {
            if (number >= 0)
                throw new TException();
        }

        /// <summary>
        /// The requirement is satisfied when the given number is less then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="message">The message that describes the error.</param>
        public static void LessThenZero<TException>(int number, string message) where TException : Exception
        {
            if (number >= 0)
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// The requirement is satisfied when the given number is less then zero.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void LessThenZero<TException>(int number, Func<TException> exceptionFactory) where TException : Exception
        {
            if (number >= 0)
                throw exceptionFactory();
        }
    }
}
