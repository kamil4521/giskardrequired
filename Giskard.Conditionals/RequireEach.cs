﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// Requires that all object in the given collection should fulfill all conditions.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all object in this collection fulfill all conditions.</param>
        /// <param name="conditions">The list of conditions.</param>
        public static void Each<TSource, TException>(IEnumerable<TSource> candidates, params Func<TSource, bool>[] conditions)
            where TException : Exception, new()
        {
            foreach (var candidate in candidates)
            {
                if (conditions.Any(condition => !condition(candidate)))
                    throw new TException();
            }
        }

        /// <summary>
        /// Requires that all object in the given collection should fullfil all conditions.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="candidates">The requirement is satisfied if all object in this collection fulfill all conditions.</param>
        /// <param name="conditions">The list of conditions.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void Each<TSource, TException>(IEnumerable<TSource> candidates, Func<TException> exceptionFactory, params Func<TSource, bool>[] conditions)
            where TException : Exception
        {
            foreach (var candidate in candidates)
            {
                if (conditions.Any(condition => !condition(candidate)))
                    throw exceptionFactory();
            }
        }
    }
}
