﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GiskardSolutions.Conditionals
{
    public partial class Require
    {
        /// <summary>
        /// Requires that collection is null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is null nor empty.</param>
        public static void IsEmpty<TSource, TException>(IEnumerable<TSource> colletion) where TException : Exception, new()
        {
            if (colletion != null && colletion.Any())
                throw new TException();
        }

        /// <summary>
        /// Requires that collection is null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is null nor empty.</param>
        /// <param name="message">The message that describes the error.</param>
        public static void IsEmpty<TSource, TException>(IEnumerable<TSource> colletion, string message) where TException : Exception
        {
            if (colletion != null && colletion.Any())
                throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        /// Requires that collection is null or empty.
        /// </summary>
        /// <typeparam name="TSource">The class of collection items.</typeparam>
        /// <typeparam name="TException">The exception to throw if the requirement is not satisfied.</typeparam>
        /// <param name="colletion">The requirement is satisfied if this is null nor empty.</param>
        /// <param name="exceptionFactory">Should construct the exception to throw if the requirement is not satisfied.</param>
        public static void IsEmpty<TSource, TException>(IEnumerable<TSource> colletion, Func<TException> exceptionFactory) where TException : Exception
        {
            if (colletion != null && colletion.Any())
                throw exceptionFactory();
        }
    }
}
